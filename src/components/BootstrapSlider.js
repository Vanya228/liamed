import React from "react";
import Carousel from "react-bootstrap/Carousel";
import Row from "react-bootstrap/Row";

import "../css/Carousel.css";

class Slider extends React.Component {
  render() {
    return (
      <Row className="mx-auto">
        <Carousel className="carousel w-100">
          <Carousel.Item>
            <img
              alt="relax"
              className="d-block w-100 h-100 img"
              src={require("../assets/lying_girl.png")}
              // alt="First slide"
            />
            {/* <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
              </Carousel.Caption> */}
          </Carousel.Item>
          {/* <Carousel.Item>
            <img
              className="d-block w-100 h-100 img"
              src={require("../assets/holiday.jpg")}
              // alt="Third slide"
            />

            {/* <Carousel.Caption>
                <h3>Second slide label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </Carousel.Caption> */}
          {/* </Carousel.Item> */}
          <Carousel.Item>
            <img
              alt="smiling"
              className="d-block w-100 h-100 img"
              src={require("../assets/smiling_girl.jpg")}
              // alt="Third slide"
            />

            {/* <Carousel.Caption>
                <h3>Third slide label</h3>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
              </Carousel.Caption> */}
          </Carousel.Item>
          
        </Carousel>
      </Row>
    );
  }
}
export default Slider;
